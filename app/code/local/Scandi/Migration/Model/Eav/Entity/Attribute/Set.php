<?php
class Scandi_Migration_Model_Eav_Entity_Attribute_Set extends Mage_Eav_Model_Entity_Attribute_Set
{
    /**
     * This rewrite sets skeleton id on model so it is possible to track the attribute set id
     * that groups are copied from
     *
     * @param int $skeletonId
     * @return Mage_Eav_Model_Entity_Attribute_Set
     */
    public function initFromSkeleton($skeletonId)
    {
        $this->setSkeletonId($skeletonId);
        return parent::initFromSkeleton($skeletonId);
    }
}