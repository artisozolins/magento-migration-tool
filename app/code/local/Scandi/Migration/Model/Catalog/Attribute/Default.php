<?php
class Scandi_Migration_Model_Catalog_Attribute_Default extends Mage_Catalog_Model_Resource_Setup
{
    /**
     * Filter out values that catalog setup already has as default values
     *
     * @param array $data
     * @return array
     */
    public function filterDefaultValues($attributeData)
    {
        /* Get default values */
        $defaultValues = $this->_prepareValues(array());
        foreach ($attributeData as $key => $data) {
            if (in_array($key, array_keys($defaultValues)) && $attributeData[$key] == $defaultValues[$key]) {
                unset($attributeData[$key]);
            }
        }
        return $attributeData;
    }
}