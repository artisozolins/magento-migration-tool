<?php
class Scandi_Migration_Model_Generator_Newsletter_Template extends Scandi_Migration_Model_Generator_Abstract
{
    /* Newsletter template models have all fields duplicated with 'template' prefix so we ignore ones without
       prefix because all database fields in table have 'template' prefix */
    protected $_ignoreFields = array('form_key',
        'template_text_preprocessed', 'added_at', 'modified_at', 'key',
        'text', 'sender_email', 'sender_name', 'subject', 'code'
    );
    protected $_htmlFields = array('template_text', 'template_styles');
    protected $_identifier = 'template_code';
}