<?php
class Scandi_Migration_Model_Generator_Cms_Page extends Scandi_Migration_Model_Generator_Abstract
{
    protected $_identifier = 'identifier';
    protected $_htmlFields = array('content', 'layout_update_xml', 'custom_layout_update_xml');
}