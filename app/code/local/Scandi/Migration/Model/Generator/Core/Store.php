<?php
class Scandi_Migration_Model_Generator_Core_Store extends Scandi_Migration_Model_Generator_Abstract
{
    protected $_identifier = 'code';

    /**
     * Load website by code rather than auto increment id
     *
     * @param $data string
     * @param $key string
     * @return string
     */
    public function getModelDataAssignString($key, $data, $asVariable = false)
    {
        if ($key == 'website_id') {
            $websiteCode = Mage::getModel('core/website')->load($data)->getCode();
            $data = "Mage::getModel('core/website')->load('{$websiteCode}', 'code')->getId()";
            $asVariable = true;
        }
        return parent::getModelDataAssignString($key, $data, $asVariable);
    }
}