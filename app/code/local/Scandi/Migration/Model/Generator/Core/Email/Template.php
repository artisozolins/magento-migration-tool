<?php
class Scandi_Migration_Model_Generator_Core_Email_Template extends Scandi_Migration_Model_Generator_Abstract
{
    protected $_ignoreFields = array('form_key', 'added_at', 'modified_at');
    protected $_identifier = 'template_code';
    protected $_htmlFields = array('template_text', 'template_styles');
}