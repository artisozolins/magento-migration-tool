<?php
class Scandi_Migration_Model_Generator_Core_Config_Data extends Scandi_Migration_Model_Generator_Abstract
{
    /* @var $_model Mage_Core_Model_Config_Data */
    protected $_model;
    protected $_identifier = 'path';
    protected $_htmlFields = 'textarea';

    /**
     * Return migration script if data has changed
     *
     * @return string
     */
    public function getSaveScript()
    {
        $result = '';
        if ($this->fieldShouldBeSaved()) {
            $value = $this->getValue();
            if ($this->_model->getFieldConfig() && $this->_model->getFieldConfig()->frontend_type == $this->_htmlFields) {
                $result .= $this->getHeredocAssignString($this->_htmlFields, $this->_model->getValue());
                $value = "\${$this->_htmlFields}";
            }
            $result .= $this->getConfigString($this->_model->getPath(), $value, $this->_model->getScope(), $this->_model->getScopeId());
        }
        return $result;
    }

    /**
     * @param string $path
     * @param string $value
     * @param string $scope
     * @param int $scopeId
     * @return string
     */
    public function getConfigString($path, $value, $scope = 'default', $scopeId = 0)
    {
        if ($scopeId != 0) {
            return "\n\$installer->setConfigData('{$path}', '{$value}', '{$scope}', '{$scopeId}');";
        } else {
            return "\n\$installer->setConfigData('{$path}', '{$value}', '{$scope}');";
        }
    }

    /**
     * Get config value that is comparable with old value
     *
     * @return string
     */
    public function getValue()
    {
        if (is_array($this->_model->getValue())) {
            return implode(',', $this->_model->getValue());
        }
        return $this->_model->getValue();
    }

    /**
     * Detect whether we need to send this field to migration script
     *
     * @return bool
     */
    protected function fieldShouldBeSaved()
    {
        if ($this->getValue($this->_model) != $this->_model->getOldValue()) {
            $currentConfig = Mage::getStoreConfig($this->_model->getPath(), $this->_model->getStoreCode());
            /* Check if we are not saving default value in database where it already exists in xml configuration */
            if (!$this->_model->getOldValue() && $this->getValue($this->_model) == $currentConfig) {
                return false;
            }
            return true;
        }
        return false;
    }
}