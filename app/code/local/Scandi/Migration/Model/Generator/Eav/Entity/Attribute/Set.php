<?php
class Scandi_Migration_Model_Generator_Eav_Entity_Attribute_Set extends Scandi_Migration_Model_Generator_Abstract
{
    protected $_ignoreFields = array(
        'entity_type_id', 'groups', 'remove_groups', 'attribute_set_id', 'skeleton_id', 'remove_attributes'
    );

    /**
     * @return string
     */
    public function getSaveScript()
    {
        if (!$this->_model->getId() && !$this->_model->getSkeletonId() || !count($this->getChangedData())) {
            return '';
        }

        if ($this->_model->getSkeletonId() && $this->_getSkeletonIsDefault()) {
            return $this->getInitFromSkeletonString($this->_getDefaultEntitySet()->getId());
        } else if ($this->_model->getSkeletonId()) {
            return $this->getInitFromSkeletonString($this->_model->getSkeletonId());
        } else {
            $result = $this->getLoadByNameString(
                $this->_model->getEntityTypeId(),
                $this->_model->getOrigData('attribute_set_name')
            );
            foreach ($this->getChangedData() as $key => $data) {
                $result .= $this->getModelDataAssignString($key, $data);
            }
            $result .= $this->getModelSaveString();
            return $result;
        }
    }

    /**
     * @return string
     */
    public function getDeleteScript()
    {
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->load($this->_model->getAttributeSetId());
        $result = $this->getLoadByNameString($attributeSet->getEntityTypeId(), $attributeSet->getAttributeSetName());
        $result .= $this->getModelDeleteString();
        return $result;
    }

    /**
     * Determine if skeleton that attribute set will be initialized from is default one
     *
     * @return bool
     */
    protected function _getSkeletonIsDefault()
    {
        if (!$this->_model->getSkeletonId()) {
            return true;
        }
        $setName = Mage::getModel('eav/entity_attribute_set')
            ->load($this->_model->getSkeletonId())->getAttributeSetName();
        return strtolower($setName) == 'default';
    }

    /**
     * @return Mage_Eav_Model_Entity_Attribute_Set|bool
     */
    protected function _getDefaultEntitySet()
    {
        return Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter($this->_model->getEntityTypeId())
            ->addFieldToFilter('attribute_set_name', 'Default')
            ->getFirstItem();
    }

    /**
     * Get load string that loads attribute set by entity type id and attribute set name
     *
     * @param int $entityTypeId
     * @param string $attributeSetName
     * @param string $assignToVariable
     * @return string
     */
    public function getLoadByNameString($entityTypeId, $attributeSetName, $assignToVariable = '')
    {
        $code = $this->getTypeCode($entityTypeId);
        $variable = "\n";
        if ($assignToVariable) {
            $variable .= "\${$assignToVariable} = ";
        }
        $result = "\n\n\$entityId = Mage::getModel('eav/entity_type')->loadByCode('{$code}')->getEntityTypeId();";
        $result .= "{$variable}Mage::getResourceModel('eav/entity_attribute_set_collection')";
        $result .= "\n    ->setEntityTypeFilter(\$entityId)";
        $result .= "\n    ->addFieldToFilter('attribute_set_name', '{$attributeSetName}')";
        $result .= "\n    ->getFirstItem()";
        if ($assignToVariable) {
            $result .= ';';
        }
        return $result;
    }

    /**
     * Get type code by type id
     *
     * @param int $typeId
     * @return string
     */
    public function getTypeCode($typeId)
    {
        return Mage::getModel('eav/entity_type')->load($typeId)->getEntityTypeCode();
    }

    /**
     * Get string that initializes set from skeleton
     *
     * @param int $skeletonId
     * @return string
     */
    public function getInitFromSkeletonString($skeletonId)
    {
        $skeletonSet = Mage::getModel('eav/entity_attribute_set')->load($skeletonId);
        $result = $this->getLoadByNameString(
            $this->_model->getEntityTypeId(),
            $skeletonSet->getAttributeSetName(),
            'skeletonSet'
        );
        $result .= $this->getModelCall($this->_model->getResourceName());
        foreach ($this->getChangedData() as $key => $data) {
            $result .= $this->getModelDataAssignString($key, $data);
        }
        $result .= "\n    ->setEntityTypeId(\$entityId)";
        $result .= $this->getModelSaveString(true);
        $result .= "\n    ->initFromSkeleton(\$skeletonSet->getAttributeSetId())";
        $result .= $this->getModelSaveString();
        return $result;
    }
}