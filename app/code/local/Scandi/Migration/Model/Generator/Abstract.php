<?php
abstract class Scandi_Migration_Model_Generator_Abstract extends Mage_Core_Model_Abstract
{
    /* @var $_model Mage_Core_Model_Abstract */
    protected $_model;
    protected $_ignoreFields = array('form_key'); // List of fields that should not be sent to migration script
    protected $_htmlFields = array('content');    // List of fields that should be defined using heredoc syntax

    /* Identifier that should be used to load model,
       preferably one that does not change ever (like cms block or cms page identifier fields) */
    protected $_identifier = 'id';


    /**
     * @param Mage_Core_Model_Abstract $model
     * @return Scandi_Migration_Model_Generator_Abstract
     */
    public function setModel($model)
    {
        $this->_model = $model;
        return $this;
    }

    /**
     * @return string
     */
    public function getSaveScript()
    {
        $changedData = $this->getChangedData();
        if (!count($changedData)) {
            return '';
        }
        $result = '';

        /**
         * HTML content can't simply be used in string so we make an exception
         * and define it before using heredoc syntax
         */
        foreach ($this->_htmlFields as $fieldName) {
            if (in_array($fieldName, array_keys($changedData))) {
                $result .= $this->getHeredocAssignString($fieldName, $changedData[$fieldName]);
            }
        }

        $result .= $this->getModelCall($this->_model->getResourceName());
        $result .= $this->getLoadString(
            $this->_model->getOrigData($this->_identifier) ?
                $this->_model->getOrigData($this->_identifier) : $this->_model->getData($this->_identifier),
            $this->_identifier
        );

        $htmlFields = $this->_htmlFields;
        foreach ($htmlFields as $fieldName) {
            if (in_array($fieldName, array_keys($changedData))) {
                $result .= $this->getModelDataAssignString($fieldName, "\${$fieldName}", true);
                unset($changedData[$fieldName]); //This prevents from content being assigned as regular data
            }
        }

        foreach ($changedData as $key => $data) {
            $result .= $this->getModelDataAssignString($key, $data);
        }

        $result .= $this->getModelSaveString();
        return $result;
    }

    /**
     * @return string
     */
    public function getDeleteScript()
    {
        $result = '';
        $result .= $this->getModelCall($this->_model->getResourceName());
        $result .= $this->getLoadString($this->_model->getData($this->_identifier), $this->_identifier);
        $result .= $this->getModelDeleteString();
        return $result;
    }

    /**
     * Get data that should be sent to migration script
     *
     * @return array
     */
    public function getChangedData()
    {
        $changedData = array();
        foreach ($this->_model->getData() as $key => $data) {
            if ($this->_model->dataHasChangedFor($key) && !in_array($key, $this->_ignoreFields)) {
                $changedData[$key] = $data;
            }
        }
        return $changedData;
    }

    /**
     * @param $alias string
     * @return string
     */
    public function getModelCall($alias)
    {
        return "\n\nMage::getModel('{$alias}')";
    }

    /**
     * @return string
     */
    public function getLoadString($value, $field)
    {
        if (!$value) {
            return '';
        }
        return "\n    ->load('{$value}', '{$field}')";
    }

    /**
     * Get camelcased data assign method
     *
     * @param $data string
     * @param $key string
     * @return string
     */
    public function getModelDataAssignString($key, $data, $asVariable = false)
    {
        $method = "\n    ->" . Mage::helper('scandi_migration')->underscoreToCamelCase('set_' . $key);

        if (is_array($data)) {
            /* Convert array to string and add 4 space indentation to it */
            $data = str_replace("\n", "\n    ", var_export($data, true));
            return $method . "({$data})";
        } else if ($asVariable) {
            return $method . "({$data})";
        }
        return $method . "('{$data}')";
    }

    /**
     * Get variable assignment string for HTML content
     *
     * @param $variable string
     * @param $data string
     * @return string
     */
    public function getHeredocAssignString($variable, $data)
    {
        return "\n\n\${$variable} = <<<EOF\n{$data}\nEOF;";
    }

    /**
     * @return string
     */
    public function getModelSaveString($ommitSemicolon = false)
    {
        $result = "\n    ->save()";
        return $ommitSemicolon ? $result : $result . ';';
    }

    /**
     * @return string
     */
    public function getModelDeleteString()
    {
        return "\n    ->delete();";
    }
}