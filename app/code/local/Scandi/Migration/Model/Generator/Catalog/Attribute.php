<?php
class Scandi_Migration_Model_Generator_Catalog_Attribute extends Scandi_Migration_Model_Generator_Abstract
{
    /* @var $_model Mage_Catalog_Model_Resource_Attribute */
    protected $_model;
    protected $_identifier = 'attribute_code';
    protected $_ignoreFields = array('form_key', 'modulePrefix', 'default_value_text', 'default_value_textarea',
        'default_value_date', 'default_value_yesno', 'attribute_code', 'entity_type_id'
    );
    /* Once attribute is created these fields shouldn't be changed so they are excluded from updating */
    protected $_createFields = array('backend_type', 'backend_model', 'frontend_model', 'frontend_input',
        'entity_type_id', 'option'
    );
    protected $_commaSeperatedFields = array('apply_to');
    protected $_changedData;

    /**
     * @return string
     */
    public function getSaveScript()
    {
        $changedData = $this->getChangedData();
        if (!count($changedData)) {
            return '';
        }

        if (!$this->_updateMode()) {
            $result = $this->getAddScript();
        } else {
            $result = $this->getUpdateScript();
        }

        return $result;
    }

    /**
     * Get script in case we are adding new attribute
     *
     * @return string
     */
    public function getAddScript()
    {
        $defaultsModel = new Scandi_Migration_Model_Catalog_Attribute_Default('catalog_setup');
        $changedData = $defaultsModel->filterDefaultValues($this->getChangedData());
        $changedData = $this->_transformAttributeData($changedData);

        $result = '';
        $result .= $this->getDeleteScript();
        $result .= "\n\$installer->addAttribute('{$this->_getEntityTypeCode()}', '{$this->_model->getAttributeCode()}'";
        $result .= ", array(";
        $i = 0;
        foreach ($changedData as $key => $data) {
            $result .= "\n    '{$key}' => ";
            if (is_array($data)) {
                $result .= $this->_getFormattedArray($data, 1);
            } else {
                $result .= "'{$data}'";
            }
            $result .= ++$i == count($changedData) ? '' : ',';
        }
        $result .= "\n));";
        return $result;
    }

    /**
     * @return string
     */
    public function getUpdateScript()
    {
        $changedData = $this->getChangedData();
        if (isset($changedData['frontend_label']) && is_array($changedData['frontend_label'])) {
            return $this->getModelUpdateScript();
        } else {
            return $this->getInstallerUpdateScript();
        }
    }

    /**
     * Get update script if every field can be updated with Mage_Eav_Model_Entity_Setup::updateAttribute method
     *
     * @return string
     */
    public function getInstallerUpdateScript()
    {
        $result = '';
        $code = $this->_model->getAttributeCode();
        foreach ($this->getChangedData() as $key => $data) {
            $result .= "\n\$installer->updateAttribute('{$this->_getEntityTypeCode()}', '{$code}', '{$key}', '{$data}');";
        }
        return $result;
    }

    /**
     * Get update script if some fields require usage of Mage_Catalog_Model_Resource_Eav_Attribute
     * class i.e. frontend_label field update when store labels are updated
     *
     * @return string
     */
    public function getModelUpdateScript()
    {
        $changedData = $this->getChangedData();

        $changedData['attribute_id'] =
            "\$installer->getAttributeId('{$this->_getEntityTypeCode()}', '{$this->_model->getAttributeCode()}')";

        $result = $this->getModelCall('catalog/resource_eav_attribute');
        if ($this->_model->getAttributeId()) {
            $result .= $this->getLoadByCodeString();
        }
        $result .= "\n    ->addData(array(";

        $i = 0;
        foreach ($changedData as $key => $data) {
            $result .= "\n        '{$key}' => ";
            if (is_array($data)) {
                $result .= $this->_getFormattedArray($data, 2);
            } else if ($key == 'attribute_id') {
                $result .= $data;
            }
            else {
                $result .= "'{$data}'";
            }
            $result .= ++$i == count($changedData) ? '' : ',';
        }
        $result .= "\n    ))";
        $result .= $this->getModelSaveString();
        return $result;
    }

    /**
     * @return string
     */
    public function getDeleteScript()
    {
        $typeCode = $this->_getEntityTypeCode();
        return "\n\n\$installer->removeAttribute('{$typeCode}', '{$this->_model->getAttributeCode()}');";
    }

    /**
     * Get data that should be sent to migration script
     *
     * @return array
     */
    public function getChangedData()
    {
        if ($this->_changedData) {
            return $this->_changedData;
        }

        /* Implode comma separated values */
        foreach ($this->_commaSeperatedFields as $field) {
            $fieldValue = $this->_model->getData($field);
            if (is_array($fieldValue)) {
                $this->_model->setData($field, implode(',', $fieldValue));
            }
        }

        $changedData = parent::getChangedData();

        /* Finding out diff for attribute titles */
        $origModel = Mage::getModel('catalog/resource_eav_attribute')
            ->loadByCode($this->_getEntityTypeCode(), $this->_model->getAttributeCode());
        $originalLabels = $origModel->getStoreLabels();
        $originalLabels[0] = $origModel->getFrontendLabel();

        foreach (Mage::app()->getStores(true) as $store) {
            if (!isset($originalLabels[$store->getId()])) {
                $originalLabels[$store->getId()] = '';
            }
        }

        $labelDiff = array();
        $newLabels = $this->_model->getFrontendLabel();

        foreach ($originalLabels as $key => $label) {
            if (!isset($newLabels[$key]) || $newLabels[$key] != $label) {
                $labelDiff[$key] = $newLabels[$key];
            }
        }

        if (count($labelDiff) == 1 && isset($labelDiff[0])) {
            $changedData['frontend_label'] = $labelDiff[0];
        } else if (!count($labelDiff)) {
            unset($changedData['frontend_label']);
        } else {
            array_filter($changedData['frontend_label']);
        }

        if ($this->_updateMode()) {
            foreach ($this->_createFields as $field) {
                unset($changedData[$field]);
            }
            return $changedData;
        }

        /* Ignore options specified in 'delete' array if we are creating new attribute */
        if (isset($changedData['option']['delete'])) {
            foreach ($changedData['option']['delete'] as $key => $value) {
                if (isset($changedData['option']['value'][$key]) && $changedData['option']['delete'][$key]) {
                    unset($changedData['option']['value'][$key]);
                }
            }
            unset($changedData['option']['delete']);
        }

        /* Check if option order contains any values  */
        if (isset($changedData['option']['order'])) {
            $changedData['option']['order'] = array_filter($changedData['option']['order']);
            if (empty($changedData['option']['order'])) {
                unset($changedData['option']['order']);
            }
        }

        if (isset($changedData['option']['value'])) {
            foreach ($changedData['option']['value'] as $key => $option) {
                $changedData['option']['value'][$key] = array_filter($option);
            }
        }

        $this->_changedData = $changedData;

        return $this->_changedData;
    }

    /**
     * This transformation is needed only when using Mage_Eav_Model_Entity_Setup::addAttribute method
     *
     * @param array $data
     * @return array
     */
    protected function _transformAttributeData($data)
    {
        $mapping = array(
            'backend_model'   => 'backend',
            'backend_type'    => 'type',
            'backend_table'   => 'table',
            'frontend_model'  => 'frontend',
            'frontend_input'  => 'input',
            'frontend_label'  => 'label',
            'source_model'    => 'source',
            'is_required'     => 'required',
            'is_user_defined' => 'user_defined',
            'default_value'   => 'default',
            'is_unique'       => 'unique',
            'is_global'       => 'global',
            'frontend_input_renderer' => 'input_renderer',
            'is_visible'     => 'visible',
            'is_searchable'  => 'searchable',
            'is_filterable'  => 'filterable',
            'is_comparable'  => 'comparable',
            'is_visible_on_front' => 'visible_on_front',
            'is_wysiwyg_enabled' => 'wysiwyg_enabled',
            'is_visible_in_advanced_search' => 'visible_in_advanced_search',
            'is_filterable_in_search' => 'filterable_in_search',
            'is_used_for_promo_rules' => 'used_for_promo_rules',
        );

        foreach ($mapping as $oldKey => $newKey) {
            if (isset($data[$oldKey])) {
                $data[$newKey] = $data[$oldKey];
                unset($data[$oldKey]);
            }
        }

        return $data;
    }

    /**
     * @param array $value
     * @return string
     */
    protected function _getFormattedArray($value, $indentationLevel = 0)
    {
        $indentation = '';
        for ($i=0; $i < $indentationLevel; $i++) {
            $indentation .= '    ';
        }
        $formatted = str_replace("  ", "    ", var_export($value, true));
        return str_replace("\n", "\n" . $indentation, $formatted);
    }

    /**
     * Check if attribute already exists
     *
     * @return bool
     */
    protected function _updateMode()
    {
        return is_numeric($this->_model->getAttributeId()) ? true : false;
    }

    /**
     * Get entity type code i.e. catalog_product
     *
     * @return string
     */
    protected function _getEntityTypeCode()
    {
        return $this->_model->getEntityType()->getEntityTypeCode();
    }

    /**
     * @return string
     */
    public function getLoadByCodeString()
    {
        return "\n    ->loadByCode('{$this->_getEntityTypeCode()}', '{$this->_model->getAttributeCode()}')";
    }
}