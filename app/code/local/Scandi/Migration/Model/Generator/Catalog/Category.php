<?php
class Scandi_Migration_Model_Generator_Catalog_Category extends Scandi_Migration_Model_Generator_Abstract
{
    protected $_identifier = 'url_key';
    protected $_htmlFields = array('description', 'custom_layout_update');
    protected $_ignoreFields = array('form_key', 'id');
    protected $_defaultValues = array(
        'display_mode' => 'PRODUCTS',
        'is_anchor' => '0',
        'custom_use_parent_settings' => '0',
        'custom_apply_to_products' => '0',
        'include_in_menu' => '1'
    );

    /**
     * Category models are protected so they are deletable only from admin area
     * so we fake it by setting secure flag manually
     *
     * @return string
     */
    public function getDeleteScript()
    {
        $result = "\n\nMage::register('isSecureArea', 1);";
        $result .= parent::getDeleteScript();
        return $result;
    }

    /**
     * Filter out default values if category has not been saved yet
     *
     * @return array
     */
    public function getChangedData()
    {
        $changedData = parent::getChangedData();
        foreach ($this->_defaultValues as $key => $value) {
            if (in_array($key, array_keys($this->_model->getData()))) {
                if (!$this->_model->getId() || !$this->_model->getOrigData($key) && $value == $this->_defaultValues[$key]) {
                    unset($changedData[$key]);
                }
            }
        }
        return $changedData;
    }

    /**
     * @return string
     */
    public function getLoadString($value, $field)
    {
        if ($this->_model->getId()) {
            return "\n    ->loadByAttribute('{$field}', '{$value}')";
        }
        return '';
    }
}