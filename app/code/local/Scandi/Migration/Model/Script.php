<?php
/**
 * @todo Refactor file usage of Varien_Io_File
 */
class Scandi_Migration_Model_Script extends Mage_Core_Model_Resource_Setup
{
    const BASE_VERSION = '0.1.0';
    const USE_INSTALL_FLAG_PATH = 'scandi/migration/use_install';

    /**
     * Type Constants redefined for 1.5 support
     */
    const TYPE_DATA_INSTALL = 'data-install';
    const TYPE_DB_INSTALL = 'install';
    const TYPE_DATA_UPGRADE = 'data-upgrade';

    protected $_scriptHeader;
    protected $_scriptFooter;

    public function __construct()
    {
        parent::__construct('scandi_migration_setup');
        $this->_scriptHeader = "<?php\n\n/* @var \$this {$this->_resourceConfig->setup->class} */\n\$installer = \$this;\n\$installer->startSetup();";
        $this->_scriptFooter = "\n\n\$installer->endSetup();";
    }

    /**
     * Get version number increased by one. Max version is 99.99.99
     *
     * @param $version string
     * @return string
     */
    protected function _getNextVersionNumber($version)
    {
        $versionArray = explode('.', $version);
        for ($i = count($versionArray)-1; $i >= 0; $i--) {
            $versionArray[$i]++;
            if ($versionArray[$i] > 99) {
                $versionArray[$i] = 0;
            } else {
                break;
            }
        }
        return implode('.', $versionArray);
    }

    /**
     * Get current script name that should be used to write changes
     *
     * @return string
     */
    protected function _getCurrentScriptName()
    {
        $configVer = (string) $this->_moduleConfig->version;
        $nextVersion = $this->_getNextVersionNumber($configVer);

        if ($this->_getUseInstall()) {
            return self::TYPE_DATA_INSTALL . '-' . $configVer . '.php';
        } else {
            return self::TYPE_DATA_UPGRADE . '-' . $configVer . '-' . $nextVersion . '.php';
        }
    }

    /**
     * Get script file handle that is ready to be written to
     *
     * @return Varien_Io_File
     */
    public function getScriptFile()
    {
        $ioObject = new Varien_Io_File();
        $this->_checkDirectories();

        $currentScriptName = $this->_getCurrentScriptName();
        $ioObject->cd($this->_getScriptDir());
        if (!$this->_scriptExists($this->_moduleConfig->version, $this->_getUseInstall())) {
            return $this->startScript($currentScriptName);
        }

        $ioObject->streamOpen($currentScriptName, 'a+');

        return $ioObject;
    }

    /**
     * Create script file and write header
     *
     * @param string $fileName
     * @return Varien_Io_File
     */
    public function startScript($fileName)
    {
        $ioObject = new Varien_Io_File();
        $ioObject->cd($this->_getScriptDir());

        if (!$this->_scriptExists(self::BASE_VERSION, true) && !$this->_dbInstallExists()) {
            $this->_setUseInstall(true);
        }

        $ioObject->streamOpen($fileName);
        $ioObject->streamWrite($this->_scriptHeader);
        return $ioObject;
    }

    /**
     * Finalize currently started script and increase version number
     *
     * @return void
     */
    public function finishCurrentScript()
    {
        $ioObject = new Varien_Io_File();
        $ioObject->cd($this->_getScriptDir());

        $currentScriptName = $this->_getCurrentScriptName();
        if (!$ioObject->fileExists($currentScriptName)) {
            return;
        }

        $ioObject->streamOpen($currentScriptName, 'a+');
        $ioObject->streamWrite($this->_scriptFooter);

        if ((string) $this->_moduleConfig->version == self::BASE_VERSION && $this->_getUseInstall()) {
            $this->_setUseInstall(false);
            return;
        }

        $ioObject->close();

        /* Update version number in modules config.xml file */
        $configDir = Mage::getModuleDir('etc', $this->_moduleConfig[0]->getName());

        $ioObject->open();
        $ioObject->cd($configDir);
        $configurationXml = $ioObject->read('config.xml');
        $ioObject->close();

        $ioObject->streamOpen('config.xml');
        $ioObject->cd($configDir);
        $ioObject->streamWrite(str_replace(
            $this->_moduleConfig->version,
            $this->_getNextVersionNumber($this->_moduleConfig->version),
            $configurationXml)
        );
        $ioObject->streamClose();

        /* Update module version in database to prevent it's execution on local machine */
        $this->_getResource()->setDbVersion(
            $this->_resourceName,
            $this->_getNextVersionNumber($this->_moduleConfig->version)
        );
    }

    /**
     * Returns true if first installation script should be used
     *
     * @return bool
     */
    protected function _getUseInstall()
    {
        if ($this->_dbInstallExists()) {
            return false;
        }

        if (!$this->_scriptExists(self::BASE_VERSION, true)) {
            return true;
        }

        return Mage::getStoreConfigFlag(self::USE_INSTALL_FLAG_PATH);
    }

    /**
     * Set whether to use first installation script in as current script
     *
     * @param $enabled bool
     */
    protected function _setUseInstall($enabled)
    {
        $setupModel = new Mage_Core_Model_Resource_Setup('core_setup');
        if ($enabled) {
            $setupModel->setConfigData(self::USE_INSTALL_FLAG_PATH, true);
        } else {
            $setupModel->setConfigData(self::USE_INSTALL_FLAG_PATH, false);
        }
    }

    /**
     * Check if upgrade script exists (if $install param is true check for install script instead)
     *
     * @param string $version
     * @param bool $install
     * @return bool
     */
    protected function _scriptExists($version, $install = false)
    {
        $ioObject = new Varien_Io_File();
        $ioObject->cd($this->_getScriptDir());
        if ($install && $ioObject->fileExists(self::TYPE_DATA_INSTALL . '-' . $version . '.php')) {
            return true;
        }
        if ($ioObject->fileExists(self::TYPE_DATA_UPGRADE . '-' . $version . '-' . $this->_getNextVersionNumber($version) . '.php')) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function _dbInstallExists()
    {
        $ioObject = new Varien_Io_File();
        $ioObject->cd($this->_getScriptDir(false));
        if ($ioObject->fileExists('mysql4-' . self::TYPE_DB_INSTALL . '-' . self::BASE_VERSION . '.php')) {
            return true;
        }
        return false;
    }

    /**
     * Get modules script directory
     *
     * @param bool $data
     * @return string
     */
    protected function _getScriptDir($data = true)
    {
        $dir = $data ? 'data' : 'sql';
        return Mage::getModuleDir(null, $this->_moduleConfig[0]->getName()) . DS . $dir . DS . $this->_resourceName;
    }

    /**
     * Check if necessary directories exist and are writable
     */
    protected function _checkDirectories()
    {
        $helper = Mage::helper('scandi_migration');
        $ioObject = new Varien_Io_File();
        $moduleDir = Mage::getModuleDir(null, $this->_moduleConfig[0]->getName());
        if (!$ioObject->isWriteable($moduleDir)) {
            throw new Exception($helper->__("Module directory (%s) is not writable", $moduleDir));
        }

        $ioObject->checkAndCreateFolder($moduleDir . DS . 'data');
        $ioObject->checkAndCreateFolder($moduleDir . DS . 'data' . DS . $this->_resourceName);
        $ioObject->checkAndCreateFolder($moduleDir . DS . 'sql');
        $ioObject->checkAndCreateFolder($moduleDir . DS . 'sql' . DS . $this->_resourceName);

        $dataDir = $moduleDir . DS . 'data' . DS . $this->_resourceName;
        if (!$ioObject->isWriteable($dataDir)) {
            throw new Exception($helper->__("Module directory (%s) is not writable", $dataDir));
        }
    }
}
