<?php
class Scandi_Migration_Model_Observer
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = Mage::helper('scandi_migration');
    }

    /**
     * Do necessary checks and call appropriate method
     *
     * @param Varien_Event_Observer $observer
     */
    public function callObserverMethod(Varien_Event_Observer $observer)
    {
        if (!$this->_helper->getMigrationRecordingEnabled()) {
            return;
        }
        $method = $this->_helper->underscoreToCamelCase($observer->getEvent()->getName());
        call_user_func(array($this, $method), $observer);
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function modelSaveBefore(Varien_Event_Observer $observer)
    {
        if ($generator = $this->getGenerator($observer->getObject())) {
            $result = $generator->getSaveScript($observer->getObject());
        } else {
            return;
        }
        $this->writeScript($result);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function modelDeleteBefore(Varien_Event_Observer $observer)
    {
        if ($generator = $this->getGenerator($observer->getObject())) {
            $result = $generator->getDeleteScript($observer->getObject());
        } else {
            return;
        }
        $this->writeScript($result);
    }


    /**
     * @param string $scriptText
     */
    public function writeScript($scriptText)
    {
        if (!$scriptText) {
            return;
        }
        $scriptObject = new Scandi_Migration_Model_Script();
        try {
            $scriptFile = $scriptObject->getScriptFile();
            $scriptFile->streamWrite($scriptText);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('scandi_migration')->__("Migration helper: ") . $e->getMessage()
            );
        }
    }

    /**
     * Get corresponding generator model
     *
     * @param Mage_Core_Model_Abstract $model
     * @return false|Scandi_Migration_Model_Generator_Abstract
     */
    public function getGenerator($model)
    {
        $ending = strtolower(str_replace('/', '_', $model->getResourceName()));
        /* If getting model fails, then we don't have an appropriate migration generator for this model */
        try {
            if ($generator = Mage::getModel('scandi_migration/generator_' . $ending)) {
                $generator->setModel($model);
            }
        } catch (Exception $e) {
            return false;
        }
        return $generator;
    }
}