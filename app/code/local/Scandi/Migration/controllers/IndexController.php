<?php
class Scandi_Migration_IndexController extends Mage_Core_Controller_Front_Action
{
    public function toggleRecordAction()
    {
        /* @var $helper Scandi_Migration_Helper_Data */
        $helper = Mage::helper('scandi_migration');
        if ($this->getRequest()->getParam('enable')) {
            $helper->setMigrationRecordingState(true);
        } else {
            $helper->setMigrationRecordingState(false);
            $scriptObject = new Scandi_Migration_Model_Script();
            $scriptObject->finishCurrentScript();
        }
    }
}