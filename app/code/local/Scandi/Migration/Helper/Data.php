<?php
class Scandi_Migration_Helper_Data extends Mage_Core_Helper_Abstract
{
    const RECORD_FLAG_PATH = 'scandi/migration/record_enabled';

    /**
     * Set migration recording flag
     *
     * @param $enabled bool
     * @return void
     */
    public function setMigrationRecordingState($enabled)
    {
        $setupModel = new Mage_Core_Model_Resource_Setup('core_setup');
        if ($enabled) {
            $setupModel->setConfigData(self::RECORD_FLAG_PATH, true);
        } else {
            $setupModel->setConfigData(self::RECORD_FLAG_PATH, false);
        }
    }

    /**
     * Detect if migration recording is enabled
     *
     * @return bool
     */
    public function getMigrationRecordingEnabled()
    {
        return Mage::getStoreConfigFlag(self::RECORD_FLAG_PATH);
    }

    /**
     * Transform underscored strings to camel case - ie. call_this_function to callThisFunction
     *
     * @param $string
     * @return string
     */
    public function underscoreToCamelCase($string)
    {
        $result = '';
        $first = true;
        foreach (explode('_', $string) as $word) {
            if ($first) {
                $result .= $word;
                $first = false;
            } else {
                $result .= ucfirst($word);
            }
        }
        return $result;
    }
}