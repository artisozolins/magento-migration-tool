MigrationHelper = Class.create();
MigrationHelper.prototype = {

    initialize: function (config) {
        this.config = config;

        if (this.recordingState === undefined) {
            this.recordingState = false;
        }

        Event.observe(this.config.toggleButton, 'click', this.toggleRecording.bind(this));
        Event.observe(this.config.viewSupportedButton, 'click', this.toggleSupport.bind(this));
    },

    toggleRecording: function () {
        if (this.toggleInProgress === true) {
            return;
        }

        var toggleUrl = this.config.toggleUrl;
        if (this.config.recordingEnabled === false) {
            toggleUrl = toggleUrl + 'enable/1/';
        }

        this.config.toggleButton.addClassName('disabled');

        new Ajax.Request(toggleUrl, {
            method:'get',
            onSuccess: function() {
                this.config.toggleInProgress = false;
                this.config.toggleButton.removeClassName('disabled');
                this.config.recordingEnabled = !this.config.recordingEnabled;
                this.updateToggleButtonText();
            }.bind(this)
        });
    },

    updateToggleButtonText: function () {
        if (this.config.recordingEnabled) {
            this.config.toggleButton.innerHTML = this.config.enabledText;
        } else {
            this.config.toggleButton.innerHTML = this.config.disabledText;
        }
    },

    toggleSupport: function () {
        Effect.toggle(this.config.viewSupportedContent, 'slide', {duration: 0.1});
    }
};